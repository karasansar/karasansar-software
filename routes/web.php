<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::post('/', [ContactController::class, 'store'])->name('contact.store');


Route::group(['middleware' => 'admin'], function () {
    Route::prefix('admin')->group(function() {
        Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
            return Inertia\Inertia::render('Dashboard');
        })->name('dashboard');
    });
});

